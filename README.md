# README #

Example game using Photon TrueSync

### What is this repository for? ###

* Give a simple example on how to use Photon TrueSync in a game. 
* Companian to article: http://blog.thedreamweb.eu/post/2017/12/22/Experimenting-with-deterministic-lockstep-multiplayer

### How do I get set up? ###

* Clone the project to a local folder;
* Import the TrueSync package from the Unity Asset Store (https://assetstore.unity.com/packages/tools/network/photon-truesync-73228);
* (Optional) Import the Unity PostProcessing stack (to pretify);
* You need to create a Photon App-id (Run the PUN Wizard in the Unity Editor);
* Use Unity to build an executable or run from the Editor;
* The most important scenes are in the Assets/Game folder: Assets/Game/Menu (matchmaking menu) and Assets/Game/Game (actual game);

### What is in the repo? ###

* An example game in the Assets/Game folder;
* Some UI assets from Kenney (www.kenney.nl);
* The Lekton font by Accademia di Belle Arti di Urbino (https://www.fontsquirrel.com/license/lekton)

### Known issues ###
The TrueSync package spams your root Assets folder with subfolders. Moving these folders tends to break TrueSync, so bear with it (for now).

### Who do I talk to? ###

* Maarten Engels (maarten@thedreamweb.eu)