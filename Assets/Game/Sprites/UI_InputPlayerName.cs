﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_InputPlayerName : MonoBehaviour {

    public string playerName = "playerName";

	public void PlayerNameOKButtonClicked()
    {
        PlayerPrefs.SetString("PlayerName", playerName);
        this.gameObject.SetActive(false);
    }

    public void PlayerNameInputFieldChanged(string value)
    {
        playerName = value;
        //Debug.Log("playerName: "+playerName);
    }

}
