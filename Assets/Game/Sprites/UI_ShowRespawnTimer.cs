﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TrueSync;

public class UI_ShowRespawnTimer : TrueSyncBehaviour {

    Text text;
    PlayerMovement player;

    public override void OnSyncedStart()
    {
        text = GetComponent<Text>();
        text.enabled = false;
        player = FindPlayer();
    }

    PlayerMovement FindPlayer()
    {
        foreach (PlayerMovement player in GameObject.FindObjectsOfType<PlayerMovement>())
        {
            if (player.owner.Id == 0 || player.owner.Id == this.localOwner.Id)
            {
                return player;
            }
        }

        return null;
    }

    public override void OnSyncedUpdate()
    {
        if (player.isRespawning)
        {
            text.enabled = true;
            text.text = "Respawning in " + ((float)player.respawnDelay).ToString("f1") + "s";
        } else
        {
            text.enabled = false;
        }
    }
}
