﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ShowPlayerStatsInGame : MonoBehaviour {

	Text text;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text> ();
	}
	
	// Update is called once per frame
	void Update () {      
		string result = string.Format("{0, -14} {1, -5} {2, -6} \n", "Player", "Kills", "Deaths") ;
		PlayerMovement[] playerMovements = GameObject.FindObjectsOfType<PlayerMovement> ();
		foreach (PlayerMovement movement in playerMovements) {
            string playerName = movement.name;
            if (movement.owner.Id != 0)
            {
                playerName = movement.owner.Name;
            }

            result += string.Format("{0, -14} {1, 5} {2, 6} \n", playerName, movement.kills, movement.deaths);

		}


		Bot[] bots = GameObject.FindObjectsOfType<Bot> ();
		foreach (Bot bot in bots) {

            result += string.Format("{0, -14} {1, 5} {2, 6} \n", bot.name, bot.kills, bot.deaths);

        }

		text.text = result;
	}
}
