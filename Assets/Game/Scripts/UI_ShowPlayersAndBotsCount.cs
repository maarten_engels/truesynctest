﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_ShowPlayersAndBotsCount : MonoBehaviour {

    Text text;

	// Use this for initialization
	void Start () {
        text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        int humanPlayers = PhotonNetwork.playerList.Length;
        int botCount = TutorialMenu.NUMBER_OF_PLAYERS_IN_MATCH - PhotonNetwork.playerList.Length;
        string result = "Joining as player: " + PhotonNetwork.player.NickName + "\n";
        result += "Human players: " + humanPlayers + " (";
        for (int i=0; i < PhotonNetwork.playerList.Length; i++)
        {
            result += PhotonNetwork.playerList[i].NickName;
            if (i < PhotonNetwork.playerList.Length - 1)
            {
                result += ", ";
            }
            result += ")\n";
        }

        result += "Bots: " + botCount;
        text.text = result;
	}
}
