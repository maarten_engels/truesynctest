﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrueSync;

public class PlayerMovement : GameActor {

    private void Start()
    {
        string playerName = "default";
        if (PlayerPrefs.HasKey("PlayerName"))
        {
            playerName = PlayerPrefs.GetString("PlayerName");
            
        }

        this.gameObject.name = playerName;
    }

    public override void OnSyncedInput() {
		FP accell = Input.GetAxis ("Vertical");
		FP steer = -Input.GetAxis ("Horizontal");

		TrueSyncInput.SetFP (0, accell);
		TrueSyncInput.SetFP (1, steer);
	}

	public override void OnSyncedUpdate () {
        base.OnSyncedUpdate();

        FP accell = TrueSyncInput.GetFP (0);
		FP steer = TrueSyncInput.GetFP (1);

		//accell *= 10 * TrueSyncManager.DeltaTime;
		steer *= 250 * TrueSyncManager.DeltaTime;

		FP rotationRadians = TSMath.Deg2Rad * tsTransform2D.rotation;
		TSVector2 dir = new TSVector2 (-TSMath.Sin (rotationRadians), TSMath.Cos (rotationRadians));

	



		tsRigidBody2D.AddForce (dir * accell * 100);
		tsRigidBody2D.MoveRotation (tsTransform2D.rotation + steer);
		//	.AddTorque (TSVector2.right * steer * 100);

		//tsTransform2D.position += dir * accell;
		//tsTransform2D.rotation += steer;
	}	

	void OnGUI() {
		//GUI.Label (new Rect(10, 100 + 30 * owner.Id, 300, 30), "player: " + owner.Id + ", deaths: " + deaths);
	}
}
