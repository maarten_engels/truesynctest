﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrueSync;

public class Bot : GameActor {

	public FP rotationDampingFactor = 4;

	public override void OnSyncedUpdate() {
        base.OnSyncedUpdate();

		FP steer = TSMath.Sin (TrueSyncManager.Time / rotationDampingFactor ) * 180;

        steer *= TrueSyncManager.DeltaTime;

        FP rotationRadians = TSMath.Deg2Rad * tsTransform2D.rotation;
        TSVector2 dir = new TSVector2(-TSMath.Sin(rotationRadians), TSMath.Cos(rotationRadians));

        tsRigidBody2D.AddForce(dir * 100);
        tsRigidBody2D.MoveRotation(tsTransform2D.rotation + steer);
    }

	
}
