﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrueSync; 

public class Projectile : TrueSyncBehaviour {

	public FP speed = 15;
	public GameObject gib;
    public GameObject projectileGib;
    public GameActor owningPlayer;

	[AddTracking]
	private FP destroyTime = 3;

	public override void OnSyncedUpdate () {
		if (destroyTime <= 0) {
			TrueSyncManager.SyncedDestroy (this.gameObject);
		}

		

		destroyTime -= TrueSyncManager.DeltaTime;
	}

    public void SetVelocity(TSVector2 velocity)
    {
        tsRigidBody2D.velocity = velocity * speed;
    }

	public void OnSyncedTriggerEnter(TSCollision2D other) {
		if (other.gameObject.tag == "Player") {
			PlayerMovement hitPlayer = other.gameObject.GetComponent<PlayerMovement> ();
			//TrueSyncManager.SyncedDestroy (this.gameObject);

			if (gib != null) {
				GameObject go = TrueSyncManager.SyncedInstantiate (gib) as GameObject;
				go.transform.position = transform.position;
			}

            owningPlayer.kills += 1;

			hitPlayer.Respawn ();
		} else if (other.gameObject.tag == "Bot") {
			Bot hitBot = other.gameObject.GetComponent<Bot> ();
			if (gib != null) {
				GameObject go = TrueSyncManager.SyncedInstantiate (gib);
				go.transform.position = transform.position;
			}

            owningPlayer.kills += 1;

            hitBot.Respawn ();
		}
        if (projectileGib != null)
        {
            GameObject go = TrueSyncManager.SyncedInstantiate(projectileGib);
            go.transform.position = transform.position;
        }
        TrueSyncManager.SyncedDestroy(this.gameObject);
    }
}
