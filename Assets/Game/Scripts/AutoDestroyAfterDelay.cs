﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyAfterDelay : MonoBehaviour {

	public float destroyDelay = 2f;

	private float delay;

	// Use this for initialization
	void Start () {
		delay = destroyDelay;
	}
	
	// Update is called once per frame
	void Update () {
		if (delay <= 0) {
			Destroy (gameObject);
		}
		delay -= Time.deltaTime;
	}
}
