﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrueSync;

public class GameActor : TrueSyncBehaviour {

    [AddTracking]
	public int deaths = 0;

    [AddTracking]
    public int kills = 0;

    [AddTracking]
    public FP respawnDelay = 0;

    [AddTracking]
    public bool isRespawning = false;
    
    public override void OnSyncedStart () {
		tsTransform2D.position = new TSVector2 (TSRandom.Range(-5,5), TSRandom.Range(-5,5));
	}

    public void Respawn()
    {
        SetVisibility(false);
        isRespawning = true;
        respawnDelay = 3;
        deaths += 1;
    }

    public override void OnSyncedUpdate()
    {
        if (isRespawning)
        {

            if (respawnDelay > 0)
            {
                respawnDelay -= TrueSyncManager.DeltaTime;
            } else
            {
                tsTransform2D.position = new TSVector2(TSRandom.Range(-5, 5), TSRandom.Range(-5, 5));
                tsTransform2D.rotation = TSRandom.Range(-180, 180);

                SetVisibility(true);
                isRespawning = false;
            }
        }
    }

    private void SetVisibility(bool visibility)
    {
        foreach (TSCollider2D col in GetComponentsInChildren<TSCollider2D>())
        {
            col.enabled = visibility;
        }

        foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
        {
            renderer.enabled = visibility;
        }
    }

    void OnGUI() {
		//GUI.Label (new Rect(10, 100 + 30 * owner.Id, 300, 30), "player: " + owner.Id + ", deaths: " + deaths);
	}
}
