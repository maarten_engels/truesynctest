﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialMenu : Photon.PunBehaviour {

    public const int NUMBER_OF_PLAYERS_IN_MATCH = 4;

    public Button StartGameButton;
    public GameObject playerNamePanel;

    string playerName;

	void Start () {
        StartGameButton.interactable = false;
        PhotonNetwork.ConnectUsingSettings ("v1.0");
		PhotonNetwork.automaticallySyncScene = true;

        if (PlayerPrefs.HasKey("PlayerName") == false)
        {
            playerNamePanel.SetActive(true);
        } else
        {
            playerName = PlayerPrefs.GetString("PlayerName");
            playerNamePanel.SetActive(false);
        }
	}

	public override void OnJoinedLobby() {
		PhotonNetwork.JoinOrCreateRoom ("room1", null, null);
        if (PhotonNetwork.isMasterClient)
        {
            StartGameButton.interactable = true;
        }

        PhotonNetwork.player.NickName = playerName;
	}

    public void Update()
    {
        if (PhotonNetwork.isMasterClient && StartGameButton.interactable == false)
        {
            StartGameButton.interactable = true;
        }
    }

    

    public void StartGame()
    {
        if (PhotonNetwork.isMasterClient)
        {
            int humanPlayers = PhotonNetwork.playerList.Length;
            int botCount = NUMBER_OF_PLAYERS_IN_MATCH - PhotonNetwork.playerList.Length;
            PlayerPrefs.SetInt("BotCount", botCount);
            PhotonNetwork.LoadLevel("Game/Game");

        }
    }
}
