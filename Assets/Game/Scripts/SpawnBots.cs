﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrueSync;

/// <summary>
/// This script spawns bots for missing human players and in offline mode.
/// Attach this to the Player prefab gameObject, 
/// so we can be sure that owner.Id is set correctly.
/// </summary>
public class SpawnBots : TrueSyncBehaviour {

	public GameObject botPrefab;

	public override void OnSyncedStart() {
		
        // The first player to join always has owner.Id == 1
        // When in offline mode, owner.Id == 0
        // In the end it doesn't really matter who spawns the bots,
        // as long as only one player spawns them. 
		if (owner.Id == 1 || owner.Id == 0) {
            // by default, we assume a situation where we are the only player 
            // and somehow BotCount was not set in PlayerPrefs.
            int botCount = TutorialMenu.NUMBER_OF_PLAYERS_IN_MATCH - 1;
            if (PlayerPrefs.HasKey("BotCount"))
            {
                botCount = PlayerPrefs.GetInt("BotCount");
            }

            for (int i = 0; i < botCount; i++)
            {
                GameObject bot = TrueSyncManager.SyncedInstantiate(botPrefab);
                bot.name = "Bot " + i;
            }
  		}
	}
}