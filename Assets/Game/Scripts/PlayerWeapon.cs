﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrueSync;

public class PlayerWeapon : TrueSyncBehaviour {

	public GameObject projectilePrefab;	

	public override void OnSyncedInput() {
		if (Input.GetButton("Fire1"))
			TrueSyncInput.SetByte (2, 1);
		else
			TrueSyncInput.SetByte (2, 0);
	}

	[AddTracking]
	private FP cooldown = 0;

	public override void OnSyncedUpdate () {
		byte fire = TrueSyncInput.GetByte (2);


		if (fire == 1 && cooldown <= 0) {

			FP rotationRadians = TSMath.Deg2Rad * tsTransform2D.rotation;
			TSVector2 dir = new TSVector2 (-TSMath.Sin (rotationRadians), TSMath.Cos (rotationRadians));

			GameObject projectileObject = TrueSyncManager.SyncedInstantiate (projectilePrefab);

			Projectile projectile = projectileObject.GetComponent<Projectile> ();
			projectileObject.GetComponent<TSTransform2D> ().position = tsTransform2D.position + dir * 1.5;
			projectile.SetVelocity(dir);
            projectile.owningPlayer = GetComponent<GameActor>();
            //projectile.owner = owner;

			cooldown = 1;
		}
		cooldown -= TrueSyncManager.DeltaTime;
	}
}
