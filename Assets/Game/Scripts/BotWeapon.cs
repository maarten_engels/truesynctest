﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrueSync;

public class BotWeapon : TrueSyncBehaviour {

	public GameObject projectilePrefab;

	[AddTracking]
	FP cooldown = 3;

    public override void OnSyncedStart()
    {
        int maxCooldown = 2 * (int)cooldown;
        cooldown = TSRandom.Range(0, 2 * maxCooldown);
    }

    public override void OnSyncedUpdate() {
		cooldown -= TrueSyncManager.DeltaTime;

		if (cooldown <= 0) {
			GameObject projectileObject = TrueSyncManager.SyncedInstantiate (projectilePrefab);

			FP rotationRadians = TSMath.Deg2Rad * tsTransform2D.rotation;
			TSVector2 dir = new TSVector2 (-TSMath.Sin (rotationRadians), TSMath.Cos (rotationRadians));

			projectileObject.GetComponent<TSTransform2D>().position = tsTransform2D.position + dir * 1.5; 

			Projectile projectile = projectileObject.GetComponent<Projectile> ();
            projectile.owningPlayer = GetComponent<GameActor>();
			projectile.SetVelocity(dir);

			cooldown = TSRandom.Range (1, 3);

		}


	}
}
